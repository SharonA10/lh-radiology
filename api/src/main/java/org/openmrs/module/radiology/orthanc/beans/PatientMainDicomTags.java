/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.orthanc.beans;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public class PatientMainDicomTags {
	
	@JsonProperty("PatientBirthDate")
	private String patientBirthDate;
	
	@JsonProperty("PatientID")
	private String patientID;
	
	@JsonProperty("PatientName")
	private String patientName;
	
	@JsonProperty("PatientSex")
	private String patientSex;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	@JsonProperty("PatientBirthDate")
	public String getPatientBirthDate() {
		return patientBirthDate;
	}
	
	@JsonProperty("PatientBirthDate")
	public void setPatientBirthDate(String patientBirthDate) {
		this.patientBirthDate = patientBirthDate;
	}
	
	@JsonProperty("PatientID")
	public String getPatientID() {
		return patientID;
	}
	
	@JsonProperty("PatientID")
	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}
	
	@JsonProperty("PatientName")
	public String getPatientName() {
		return patientName;
	}
	
	@JsonProperty("PatientName")
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	@JsonProperty("PatientSex")
	public String getPatientSex() {
		return patientSex;
	}
	
	@JsonProperty("PatientSex")
	public void setPatientSex(String patientSex) {
		this.patientSex = patientSex;
	}
	
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}
	
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
	
}
